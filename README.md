# Organize Your Toys!

A game prototype developed using Unity3D, by Murilo Luz.

![prototype](/captura.PNG)

The player will get a random toy(primitive) and the goal is to throw
the toy in the correct box.
The boxes will be placed far from the player.
By holding the left mouse button the player will be able to alter the
force applied to the throw.
Additionally, the player will be able to rotate the camera with the
mouse like a FPS. But won't be able to move.

## Installation

Download Unity Hub and Unity Editor version 2020.3.26f1.
Download all files and unzip them. 
Open Unity Hub and click Open project.
Select the unzipped folder and the correct Unity version.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](/LICENSE)
