using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool : MonoBehaviour
{

    public int amountToPool;
    public List<GameObject> objectsToPool;
    public List<GameObject> pooledObjects;
    
    // Start is called before the first frame update
    void Start()
    {
        pooledObjects = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < amountToPool; i++)
        {
            tmp = Instantiate(objectsToPool[Random.Range(0, objectsToPool.Count)], transform);
            //tmp.SetActive(false);
            tmp.transform.localPosition = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10));
            pooledObjects.Add(tmp);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < amountToPool; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }
        Debug.LogWarning("Unable to get pooled object");
        return null;
    }

    public GameObject GetNextObject()
    {
        if (transform.childCount > 0)
            return transform.GetChild(0).gameObject;

        Debug.Log("Pool Empty");
        GameManager.SharedInstance.UpdateWarnText("No more Toys");
        return null;
    }
}
