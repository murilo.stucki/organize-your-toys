using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager SharedInstance;
    [SerializeField] TextMeshProUGUI counterText;
    [SerializeField] TextMeshProUGUI warnText;

    private int count;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        if (counterText == null)
            counterText = GameObject.Find("Counter Text").GetComponent<TextMeshProUGUI>();
        if (warnText == null)
            warnText = GameObject.Find("Warn Text").GetComponent<TextMeshProUGUI>();
    }

    private void Awake() 
    {
        SharedInstance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCount(int numToAdd)
    {
        count += numToAdd;
        counterText.text = ("Count: " + count);
    }

    public void UpdateWarnText(string textToShow)
    {
        StartCoroutine(ChangeWarnTextForSeconds(textToShow, 2));
    }

    IEnumerator ChangeWarnTextForSeconds(string textToChange, float seconds)
    {
        warnText.text = textToChange;
        yield return new WaitForSeconds(seconds);
        warnText.text = "";
    }
}
