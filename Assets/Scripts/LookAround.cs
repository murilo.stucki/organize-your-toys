using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAround : MonoBehaviour
{

    private GameObject playerCamera;

    public float xSensitivity = 100f;
    public float ySensitivity = 100f;

    private float verticalRotation;
    private float mouseX = 0;
    private float mouseY = 0;


    // Start is called before the first frame update
    void Start()
    {
        verticalRotation = 0f;
        Cursor.lockState = CursorLockMode.Locked;

        playerCamera = GameObject.Find("Player Camera");
        if (!playerCamera.transform.IsChildOf(transform))
        {
            playerCamera.transform.SetParent(transform);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        mouseX = Input.GetAxis("Mouse X") * xSensitivity * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * ySensitivity * Time.deltaTime;

        verticalRotation -= mouseY;
        verticalRotation = Mathf.Clamp(verticalRotation, -90f, 90f);

        playerCamera.transform.localRotation = Quaternion.Euler(verticalRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }

    
}
